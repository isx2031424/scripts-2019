#! /bin/bash
#asx m01 diego sanchez piedra
#gener 2020
#exemple if
#	$prog edat
#--------------------------------

#validar argument
if [ $# -ne 1 ]; then
	echo "error nº d'args incorrecte $#"
	exit 1
fi

#xixa
edat=$1
if [ $edat -ge 18 ]; then
	echo "edad $edat es major d'edat"
fi
exit 0

